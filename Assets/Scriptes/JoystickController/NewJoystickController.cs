﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

public class NewJoystickController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    //StaticValuesForGame mStaticValuesForGame;
    public enum AxisOption
    {
        // Options for which axes to use
        Both, // Use both
        OnlyHorizontal, // Only horizontal
        OnlyVertical // Only vertical
    }
    public enum HeroesActionTypeEnum //Действия героя при клике на кнопку.
    {
        ShieldDefence = 1,
        SwordAttack = 2,
    }
    public enum JoystickTypeEnum
    {
        MoveJoystick = 1,
        RotateJoystick = 2
    }
    public int MovementRange = 100;
    public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
    public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
    public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input
    //public SkillsTypeEnum HeroesActionType;
    public JoystickTypeEnum JoystickType;
    [Header("Коэффициент замедления поворота")] [SerializeField] float DecelerationFactorX = 1f;
    [Header("Коэффициент замедления поворота")] [SerializeField] float DecelerationFactorY = 1f;
    Vector3 m_StartPos;
    Vector3 m_StartPos_new;
    bool m_UseX; // Toggle for using the x axis
    bool m_UseY; // Toggle for using the Y axis
    CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
    CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input
    bool ClassInitiate;

    bool ButtonPress; //Говорим о том что кнопка нажата и удары должны идти постоянно

    [SerializeField] bool RightJoystickInitiate;
    [SerializeField] Transform RightJoystickTransform;
    [SerializeField] Transform HeroesCameraTransform;

    [Header("Это дополнительный, не основной джойстик")] [SerializeField] bool ThisAHelpJoystick;
    [Header("Новая скорость игры")] [SerializeField] float NewTimeScale;
    [SerializeField] bool ThisRotateAndAttack;
    //ListButtonExecuteCod mListButtonExecuteCod = null;
    //CellActionPanelController mCellActionPanelController = null;
    Vector3 Vector3zero = Vector3.zero;

    void OnEnable()
    {
        InitiateClass();
        CreateVirtualAxes();
    }

    void Start()
    {
        m_StartPos = transform.position;
        m_StartPos_new = transform.position;
        InitiateClass();
    }

    void InitiateClass()
    {
        if (ClassInitiate) { return; }
        /*mStaticValuesForGame = GameObject.Find("RepositoryOfScripts").GetComponent<StaticValuesForGame>();
        mStaticValuesForGame.InitiateClass();

        mListButtonExecuteCod = mStaticValuesForGame.mActionPanelController.gameObject.GetComponent<ListButtonExecuteCod>();
        mCellActionPanelController = gameObject.transform.parent.GetComponent<CellActionPanelController>();*/

        //HeroesActionType = 0;

        /*if (mCellActionPanelController != null)
        {
            HeroesActionType = mCellActionPanelController.mButtonForActionPanel.SkillsType;
        }*/

        ClassInitiate = true;
    }

    void UpdateVirtualAxes(Vector3 value)
    {

        if (JoystickType == JoystickTypeEnum.RotateJoystick)
        {
            return;
        }

        if (JoystickType == JoystickTypeEnum.RotateJoystick)
        {
            /*if (mStaticValuesForGame.ActiveJoystickRotate != this) return;
            else
            {
                m_HorizontalVirtualAxis = mStaticValuesForGame.m_HorizontalVirtualAxis;
                m_VerticalVirtualAxis = mStaticValuesForGame.m_VerticalVirtualAxis;
            }*/
        }

        var delta = m_StartPos_new - value;
        delta.y = -delta.y;
        delta /= MovementRange;

        if (m_UseX)
        {
            m_HorizontalVirtualAxis.Update(-delta.x / DecelerationFactorX);
        }

        if (m_UseY)
        {
            m_VerticalVirtualAxis.Update(delta.y / DecelerationFactorY);
        }

        /*if (mStaticValuesForGame.IsometricModeEnabled)
        {
            
        }
        else
        {
            if (m_UseX)
            {
                m_HorizontalVirtualAxis.Update(-delta.x / DecelerationFactorX);
            }

            if (m_UseY)
            {
                m_VerticalVirtualAxis.Update(delta.y / DecelerationFactorY);
            }
        }

        if (JoystickType == JoystickTypeEnum.MoveJoystick && HeroesCameraTransform != null)
        {
            foreach (var GreateHealthbar in mStaticValuesForGame.ListOfGreateHealthbar)
            {
                GreateHealthbar.AlignCamera(HeroesCameraTransform);
            }
        }*/
    }

    void CreateVirtualAxes()
    {

        // set axes to use
        m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
        m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

        if (ThisAHelpJoystick) return;

        // create new axes based on axes to use
        if (m_UseX)
        {
            if (JoystickType == JoystickTypeEnum.MoveJoystick)
            {
                CrossPlatformInputManager.UnRegisterVirtualAxis(horizontalAxisName);
                m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
            }
            else
            {
                if (ClassInitiate == false) InitiateClass();

                /*if (mStaticValuesForGame.m_HorizontalVirtualAxis == null)
                {
                    CrossPlatformInputManager.UnRegisterVirtualAxis(horizontalAxisName);
                    mStaticValuesForGame.m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
                    CrossPlatformInputManager.RegisterVirtualAxis(mStaticValuesForGame.m_HorizontalVirtualAxis);
                }
                m_HorizontalVirtualAxis = mStaticValuesForGame.m_HorizontalVirtualAxis;*/
            }

        }

        if (m_UseY)
        {

            if (JoystickType == JoystickTypeEnum.MoveJoystick)
            {
                CrossPlatformInputManager.UnRegisterVirtualAxis(verticalAxisName);
                m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
            }
            else
            {
                /*if (mStaticValuesForGame.m_VerticalVirtualAxis == null)
                {
                    CrossPlatformInputManager.UnRegisterVirtualAxis(verticalAxisName);
                    mStaticValuesForGame.m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
                    CrossPlatformInputManager.RegisterVirtualAxis(mStaticValuesForGame.m_VerticalVirtualAxis);
                }
                m_VerticalVirtualAxis = mStaticValuesForGame.m_VerticalVirtualAxis;*/
            }


        }
    }

    public void OnDrag(PointerEventData data)
    {
        Vector3 newPos = Vector3.zero;

        if (m_UseX)
        {
            int delta = (int)(data.position.x - m_StartPos_new.x);
            delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
            newPos.x = delta;
        }

        if (m_UseY)
        {
            int delta = (int)(data.position.y - m_StartPos_new.y);
            delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
            newPos.y = delta;
        }

        transform.position = new Vector3(m_StartPos_new.x + newPos.x, m_StartPos_new.y + newPos.y, m_StartPos_new.z + newPos.z);
        
        if (ThisRotateAndAttack)
        {
            if (newPos.x < 10 && newPos.x > -10) newPos.x = 0;
            if (newPos.y < 10 && newPos.y > -10) newPos.y = 0;
        }

        /*if (mStaticValuesForGame.IsometricModeEnabled && JoystickType == JoystickTypeEnum.MoveJoystick)
        {
            newPos = new Vector3(newPos.y, newPos.x * -1, 0);
        }*/
        newPos = new Vector3(newPos.y, newPos.x * -1, 0);


        if (newPos == Vector3zero)
        {
            UpdateVirtualAxes(m_StartPos_new);
            Debug.Log(m_StartPos_new.ToString());
        }
        else
        {
            UpdateVirtualAxes(transform.position);
        }

        //if (RightJoystickInitiate) RightJoystickTransform.position = data.position;

    }

    public void OnPointerUp(PointerEventData data)
    {

        transform.position = m_StartPos;

        if (RightJoystickInitiate) RightJoystickTransform.localPosition = new Vector3(0, 0, 0);

        UpdateVirtualAxes(m_StartPos_new);

        /*if (HeroesActionType == SkillsTypeEnum.BaseAttack)
        {
            ButtonPress = false;
        }
        else if (HeroesActionType == SkillsTypeEnum.BaseDefenceShield)
        {
            if (mStaticValuesForGame.mSkillsController.SkillCanBeUse(mStaticValuesForGame.TekActiveHeroes, HeroesActionType))
            {

                mStaticValuesForGame.TekMoveAndAttack.TheBlockingModeEnable = false;
                mStaticValuesForGame.TekMoveAndAttack.SetTrigerAndSpeedAnimator();
            }
        }
        else if (HeroesActionType != 0)
        {
            if (NewTimeScale != 0) Time.timeScale = 1;

            if (mStaticValuesForGame.mSkillsController.SkillCanBeUse(mStaticValuesForGame.TekActiveHeroes, HeroesActionType))
            {
                mStaticValuesForGame.TekMoveAndAttack.SetTrigerAndSpeedAnimator();
                mListButtonExecuteCod.ClickButtonActionPanel(mCellActionPanelController);
            }
        }

        mStaticValuesForGame.RemoveTutorialTarget(gameObject);*/

    }

    public void OnPointerDown(PointerEventData data)
    {

        if (RightJoystickInitiate)
        {
            RightJoystickTransform.position = data.position;
            m_StartPos_new = data.position;
        }

        /*if (JoystickType == JoystickTypeEnum.RotateJoystick)
        {
            mStaticValuesForGame.ActiveJoystickRotate = this;

            if (ThisRotateAndAttack)
            {
                DecelerationFactorX = 20 - mStaticValuesForGame.mSaveSettings.SpeedRotationAttackingX;
                DecelerationFactorY = 20 - mStaticValuesForGame.mSaveSettings.SpeedRotationAttackingY;
            }
            else
            {
                DecelerationFactorX = 20 - mStaticValuesForGame.mSaveSettings.SpeedRotationX;
                DecelerationFactorY = 20 - mStaticValuesForGame.mSaveSettings.SpeedRotationY;
            }

            if (DecelerationFactorX == 0) DecelerationFactorX = 1;
            if (DecelerationFactorY == 0) DecelerationFactorY = 1;
        }*/

        /*if (HeroesActionType == SkillsTypeEnum.BaseAttack)
        {
            ButtonPress = true;
            StartCoroutine(UpgradeTargetForUnit());
        }
        else if (HeroesActionType == SkillsTypeEnum.BaseDefenceShield)
        {
            if (mStaticValuesForGame.mSkillsController.SkillCanBeUse(mStaticValuesForGame.TekActiveHeroes, HeroesActionType))
            {
                mStaticValuesForGame.TekMoveAndAttack.TheBlockingModeEnable = true;
                mStaticValuesForGame.TekMoveAndAttack.SetTrigerAndSpeedAnimator();
            }
        }
        else if (HeroesActionType != 0 && NewTimeScale != 0)
        {
            Time.timeScale = NewTimeScale;
        }*/

    }

    IEnumerator UpgradeTargetForUnit()
    {
        yield return new WaitForSeconds(0.1f);

        /*if (mStaticValuesForGame.mSkillsController.SkillCanBeUse(mStaticValuesForGame.TekActiveHeroes, HeroesActionType))
        {
            mStaticValuesForGame.TekMoveAndAttack.StartAnimationForAtack();
            mStaticValuesForGame.TekMoveAndAttack.AddSkillBaseAttack(HeroesActionType);
        }

        if (ButtonPress)
        {
            StartCoroutine(UpgradeTargetForUnit());
        }*/

    }

    void OnDisable()
    {

        //if (ThisAHelpJoystick) return;

        // remove the joysticks from the cross platform input
        if (m_UseX)
        {
            m_HorizontalVirtualAxis.Remove();
            //mStaticValuesForGame.m_HorizontalVirtualAxis = null;
        }
        if (m_UseY)
        {
            m_VerticalVirtualAxis.Remove();
            //mStaticValuesForGame.m_VerticalVirtualAxis = null;
        }

        if (NewTimeScale != 0) Time.timeScale = 1;

    }

}
