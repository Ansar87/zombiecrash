﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFindController : MonoBehaviour
{

    [SerializeField] OtherUnitsMove ParentOtherUnitsMove;
    GamerTypeEnum ThisGamerType;
    bool ThisGamer;

    // Start is called before the first frame update
    void Start()
    {
        ParentOtherUnitsMove = GetComponentInParent<OtherUnitsMove>();
        CharacterProperties ThisCharacterProperties = GetComponentInParent<CharacterProperties>();
        ThisGamerType = ThisCharacterProperties.GamerType;

        ThisGamer = (ThisGamerType == GamerTypeEnum.Gamer);
    }

    void OnTriggerEnter(Collider EnemyColider)
    {

        //if (ThisGamer) return;
        if (ParentOtherUnitsMove == null) return;

        CharacterProperties EnemyCharacterController = EnemyColider.gameObject.GetComponent<CharacterProperties>();

        if (EnemyCharacterController != null)
        {
            if (EnemyCharacterController.GamerType == ThisGamerType) return;
            ParentOtherUnitsMove.AddInTargetList(EnemyCharacterController);
        }
            

    }

    //When the Primitive exits the collision, it will change Color
    private void OnTriggerExit(Collider EnemyColider)
    {
        if (ParentOtherUnitsMove == null) return;

        CharacterProperties EnemyCharacterController = EnemyColider.gameObject.GetComponent<CharacterProperties>();

        if (EnemyCharacterController != null)
        {
            if (EnemyCharacterController.GamerType == ThisGamerType) return;
            ParentOtherUnitsMove.DeleteInTargetList(EnemyCharacterController);

            if (ThisGamer)
            {
                EnemyCharacterController.SelectedTarget(false);
            }

        }
    }

}
