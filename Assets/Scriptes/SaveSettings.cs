﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class SaveSettings : MonoBehaviour
{
    ParametrOfTheGame GameSaveValue;
    bool ClassInitiate = false;

    // Use this for initialization
    void Start()
    {
        InitiateClass();
    }

    public void InitiateClass()
    {

        if (ClassInitiate) { return; }

        if (PlayerPrefs.HasKey("SaveCountNumber"))
        {
            string SaveConquestElcastle = PlayerPrefs.GetString("SaveCountNumber");
            GameSaveValue = JsonUtility.FromJson<ParametrOfTheGame>(SaveConquestElcastle);
        }
        else
        {
            GameSaveValue = new ParametrOfTheGame();
            GameSaveValue.MaxOpenLevel = 1;
            GameSaveValue.CountOfCoins = 100;
            GameSaveValue.PlayerID = UnityEngine.Random.Range(1,10000);
        }

        if (!GameSaveValue.StartForVersion23)
        {
            GameSaveValue.StartForVersion23 = true;
            GameSaveValue.PlayerID = UnityEngine.Random.Range(1, 10000);
            GameSaveValue.CountOfCoins = 100;
            GreateListSavedScore();
        }

        ClassInitiate = true;

    }

    void GreateListSavedScore()
    {
        GameSaveValue.ListSavedScore = new List<ElementListSavedScore>();

        for (int i = 0; i < 15; i++)
        {
            ElementListSavedScore NewElement = new ElementListSavedScore();
            NewElement.id = UnityEngine.Random.Range(1, 10000);
            NewElement.Score = UnityEngine.Random.Range(2000, 30000);

            GameSaveValue.ListSavedScore.Add(NewElement);
        }
    }

    public void SaveParametrs()
    {
        string scstring = JsonUtility.ToJson(GameSaveValue);
        PlayerPrefs.SetString("SaveCountNumber", scstring);
    }

    public int GetMaxOpenLevel()
    {
        return GameSaveValue.MaxOpenLevel;
    }

    public void SetMaxOpenLevel(int NewLevel)
    {
        if (NewLevel > GameSaveValue.MaxOpenLevel)
        {
            GameSaveValue.MaxOpenLevel = NewLevel;
            SaveParametrs();
        }
    }

    public int GetNumberOfAdImpressions()
    {
        int ReturnValue = 0;

        if (GameSaveValue.DontShowADS)
        {
            ReturnValue = 0;
        }
        else
        {
            ReturnValue = GameSaveValue.NumberOfAdImpressions;
        }

        return ReturnValue;
    }

    public void AddNumberOfAdImpressions()
    {
        GameSaveValue.NumberOfAdImpressions = GameSaveValue.NumberOfAdImpressions + 1;
        SaveParametrs();
    }

    public void RestartNumberOfAdImpressions()
    {
        GameSaveValue.NumberOfAdImpressions = 0;
        SaveParametrs();
    }

    public void SetDontShowADS()
    {
        GameSaveValue.DontShowADS = true;
        SaveParametrs();
    }

    public bool GetDontShowADS()
    {
        return GameSaveValue.DontShowADS;
    }

    public int GetCountCoins()
    {
        return GameSaveValue.CountOfCoins;
    }

    public void AddCountCoins(int CoincCount)
    {
        GameSaveValue.CountOfCoins = GameSaveValue.CountOfCoins + CoincCount;
        SaveParametrs();
    }

    public int GetPlayerID()
    {
        return GameSaveValue.PlayerID;
    }

    public List<ElementListSavedScore> GetListSavedScore()
    {
        return GameSaveValue.ListSavedScore;
    }

    public bool GetClickButtonRateGame()
    {
        return GameSaveValue.ClickButtonRateGame;
    }

    public void SetClickButtonRateGame()
    {
        GameSaveValue.ClickButtonRateGame = true;
        SaveParametrs();
    }

}

[System.Serializable]
public class ParametrOfTheGame
{
    public int MaxOpenLevel; //
    public int NumberOfAdImpressions;
    public bool DontShowADS;
    public int CountOfCoins;
    public int PlayerID;
    public bool StartForVersion23;
    public List<ElementListSavedScore> ListSavedScore;
    public bool ClickButtonRateGame;

}

