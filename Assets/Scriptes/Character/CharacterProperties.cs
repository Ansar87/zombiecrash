﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterProperties : MonoBehaviour
{

    public GamerTypeEnum GamerType;
    public bool IsDead;

    [Header("Справочные показатели")]
    public int CountAttackAnimationZombie;
    public int CountAttackAnimationUnarmed;
    public float AttackRangeZombie;
    public float AttackRangeUnarmed;
    public float AttackTimeZombie;
    public float AttackTimeUnarmed;

    [Header("Материалы")]
    public Material ZombieMaterial;
    public Material GamerMaterial;

    [Header("Система частиц")]
    public GameObject TargetSelected;

    public bool ThisZombie;

    Animator mAnimator;

    void Start()
    {
        mAnimator = GetComponent<Animator>();

        AttackRangeZombie  = 1f;
        AttackRangeUnarmed = 1f;
        CountAttackAnimationZombie  = 1;
        CountAttackAnimationUnarmed = 3;
        AttackTimeZombie = 3f;
        AttackTimeUnarmed = 1.5f;
        SetMoveType();
        SetMaterial();
    }

    void Update()
    {
        
    }

    public void SetMoveType()
    {
        if (ThisZombie)
        {
            mAnimator.SetBool("Zombie", true);
            return;
        }

        if (GamerType == GamerTypeEnum.Gamer)
        {
            mAnimator.SetBool("Unarmed", true);
        }
        else if (GamerType == GamerTypeEnum.Enemy)
        {

        }
        
    }

    public void SetMaterial()
    {
        Material SelectedMaterial = null;

        if (ThisZombie)
        {
            SelectedMaterial = ZombieMaterial;
        }
        else
        {
            SelectedMaterial = GamerMaterial;
        }

        GetComponentInChildren<SkinnedMeshRenderer>().material = SelectedMaterial;

    }

    public void SelectedTarget(bool SetValue)
    {
        TargetSelected.SetActive(SetValue);

        if (!ThisZombie)
        {
            Debug.Log("sdfsf");
        }

    }

    

}
