﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class OtherUnitsMove : MonoBehaviour
{

    SoundController mSoundController;
    StaticValuesForGame mStaticValuesForGame;
    HeroesMoveController mHeroesMoveController;
    EnemyAndResourcesGeneration mEnemyAndResourcesGeneration;

    //**************СВОЙСТВА ЦЕЛИ**************\\
    [Header("Свойства цели")]
    public List<CharacterProperties> TargetList;
    public Vector3 TargetMove; //Позиция цели
    public Transform TargetAttack; //Трансформ цели
    public CharacterProperties TargetCharacterProperties;

    //**************СВОЙСТВА ДВИЖЕНИЯ**************\\
    [Header("Свойства движения")]
    //Если истина то объект движется, в противном случае стоит на месте.
    public bool IsMove;
    public float Speed;//Скорость движения
    NavMeshAgent mNavMeshAgent;//Агент для движения
    Animator mAnimator; //Аниматор
    string SetTriger; //Установленная анимация

    [Header("Свойства персонажа")]
    public CharacterProperties mCharacterProperties;
    //Номер группы персонажа
    public GamerTypeEnum GamerType;

    [Header("Свойства атаки")]
    [SerializeField] float TimeForLastAtack; // Время которое прошло с последней атаки.
    [SerializeField] float TimeBetweenAtacks; //Рассчитаное время между атаками
    [SerializeField] float AtackSpeed; //Скорость атаки
    [SerializeField] float BaseAtackSpeed; //Базовая скоросто атаки

    [Header("Свойства атаки")]
    [SerializeField] float remainingDistance;
    [SerializeField] float AttackRange;
    [SerializeField] float CountAttackAnimation;

    bool MustStandStill; //Юнит должен стоять на месте.

    float TimeBetweenRouteChecks; // Время прошедшее с последней проверки маршрута

    float LastTimeUpdate = 0;

    bool ThisGamer;

    // Start is called before the first frame update
    void Start()
    {
        GameObject GeneralScript = GameObject.FindGameObjectWithTag("GeneralScript");

        mEnemyAndResourcesGeneration = GeneralScript.GetComponent<EnemyAndResourcesGeneration>();


        mNavMeshAgent = GetComponent<NavMeshAgent>();
        mAnimator = GetComponent<Animator>();
        SetTriger = "";

        mCharacterProperties = GetComponent<CharacterProperties>();
        ThisGamer = (mCharacterProperties.GamerType == GamerTypeEnum.Gamer);

        TargetList = new List<CharacterProperties>();

        if (ThisGamer)
        {
            mNavMeshAgent.enabled = false;
            return;
        }
            

        

        UpdateTarget();

    }

    // Update is called once per frame
    void Update()
    {

        if (ThisGamer) return;

        if (mCharacterProperties.IsDead)
        {
            if (mNavMeshAgent.enabled)
            {
                mNavMeshAgent.ResetPath();
            }
            IsMove = false;
            TargetAttack = null;
            return;
        }

        LastTimeUpdate += Time.deltaTime;


        if (mNavMeshAgent.enabled == false)
        {
            return;
        }

        if (IsMove)
        {
            MustStandStill = false;
            TimeForLastAtack += Time.deltaTime * 1;
            float InsertAttackRange = 0;

            remainingDistance = mNavMeshAgent.remainingDistance;
            //AttackRange = mNavMeshAgent.stoppingDistance;

            if (mNavMeshAgent.remainingDistance != 0 && mNavMeshAgent.remainingDistance <= mNavMeshAgent.stoppingDistance + InsertAttackRange)
            {

                MustStandStill = true;

                if (TargetCharacterProperties != null)
                {

                    if (TimeForLastAtack >= TimeBetweenAtacks)
                    {

                        bool TargetActive = false;
                        TargetActive = TargetCharacterProperties.gameObject.activeInHierarchy;

                        if (TargetActive)
                        {
                            StartAnimationForAtack();
                            TimeForLastAtack = 0;
                            mNavMeshAgent.ResetPath();
                        }
                    }
                    else
                    {
                        SetTrigerAndSpeedAnimator();
                    }
                }
                else
                {
                    UpdateTarget();
                    SetTrigerAndSpeedAnimator();
                    IsMove = false;
                    mNavMeshAgent.ResetPath();
                }
            }
            else if (TargetCharacterProperties == null && mNavMeshAgent.remainingDistance != 0 && mNavMeshAgent.remainingDistance <= 3)
            {
                UpdateTarget();
            }
            else
            {

                if (mNavMeshAgent.remainingDistance == 0 && TargetMove != null && Vector3.Distance(TargetMove, transform.position) < mNavMeshAgent.stoppingDistance)
                {
                    MustStandStill = true;
                    IsMove = false;
                    SetTrigerAndSpeedAnimator();
                    mNavMeshAgent.ResetPath();
                }

                if (MustStandStill && TargetAttack != null)
                {
                    CharacterMove(TargetAttack.position, TargetAttack);
                    return;
                }

                TimeBetweenRouteChecks += Time.deltaTime;

                if (TimeBetweenRouteChecks >= 1f && TargetAttack != null)
                {
                    TimeBetweenRouteChecks = 0f;
                    CharacterMove(TargetAttack.position, TargetAttack);

                }

            }

        }

    }

    public void CharacterMove(Vector3 _TargetMove, Transform TargetTransform = null)
    {

        TargetMove = _TargetMove;
        TargetAttack = null;
        TargetCharacterProperties = null;
        
        IsMove = false;

        if (mCharacterProperties.IsDead) { return; }

        if (TargetTransform == null)
        {
            // Нету колайдера, значит это не наша цель. просто идём к точке куда кликнули.
            MoveTarget(_TargetMove);
            // Обнуляем цели атаки.
            mNavMeshAgent.stoppingDistance = 0f;
            
        }
        else
        {
            SetTargetAtack(TargetTransform); //Устанавливаем цель для атаки.        
        }

    }

    IEnumerator UnitUpgradeTargetPosition()
    {

        yield return new WaitForSeconds(0.5f);

        if (mCharacterProperties.IsDead)
        {
            yield return null;
        }

        if (mCharacterProperties.IsDead == false
        && TargetAttack != null
        && TargetMove != null)
        {
            //float DistanceVector = Vector3.Distance(TargetMove, TargetAttack.position);
            float DistanceVector = Vector3.Distance(transform.position, TargetAttack.position);

            if (DistanceVector > AttackRange)
            {
                TargetMove = TargetAttack.position;
                StartCoroutine(UnitMoveForTarget());
            }

        }

        if (mCharacterProperties.IsDead == false
        && TargetAttack != null)
        {
            StartCoroutine(UnitUpgradeTargetPosition());
        }

    }

    IEnumerator UnitMoveForTarget()
    {

        if (mNavMeshAgent.enabled && mNavMeshAgent.SetDestination(TargetMove))
        {

            while (mNavMeshAgent.pathPending)
            {
                yield return null;
            }

            SetTrigerAndSpeedAnimator();
            IsMove = true;
        }

    }

    public void MoveTarget(Vector3 _TargetMove)
    {
        MustStandStill = false;
        StartCoroutine(UnitMoveForTarget());
    }

    public void SetTargetAtack(Transform _TargetAttack)
    {

        //Устанавливаем цель.
        TargetAttack = _TargetAttack;

        //Получаем свойства цели
        TargetCharacterProperties = TargetAttack.GetComponent<CharacterProperties>();

        //Установим дистанцию атаки
        //SetStopingDistance();
        
        //Говорим что нужно идти к цели
        MoveTarget(_TargetAttack.position);

        //Запускаем автообновление цели.
        StartCoroutine(UnitUpgradeTargetPosition());
    }

    public void StartAnimationForAtack()
    {

        transform.LookAt(TargetAttack);
        
        mAnimator.SetInteger("ActionNumber", Random.Range(0, GetCountAttackAnimation()));
        mAnimator.SetTrigger("BaseAttack");

        /*if (mCharacterProperties.IsArcher)
        {
            StartCoroutine(SendArrowInFlith());
            StartSoundEffect();
        }
        else if (mCharacterProperties.IsMage)
        {
            StartCoroutine(SendMageMissale());
        }*/

        StartSoundEffect();
    }

    public void StopAnimationForAttack()
    {
        //AttackAllowed = false;
        //mAnimator.SetBool(mCharacterProperties.ParametrName, false);
    }

    public void StopNavMeshAgent(bool SetValue)
    {
        TargetAttack = null;
        IsMove = false;

        if (SetValue)
        {
            mNavMeshAgent.isStopped = true;
            mNavMeshAgent.ResetPath();
        }

        mNavMeshAgent.enabled = !SetValue;

    }

    public void StartSoundEffect()
    {
        /*if (mCharacterProperties.IsArcher)
        {
            mSoundController.ToPlayMusic(mAudioSource, mSoundController.BowShoot, false, 15, 1f);
        }
        else
        {
            mSoundController.ToPlayMusic(mAudioSource, mSoundController.SwordImpact, false, 15, 1f);
        }*/
    }

    public void SetTrigerAndSpeedAnimator()
    {
        /*string TriggerName = GetTriggerName();

        if (SetTriger != TriggerName)
        {
            //if (SetTriger != "" || SetTriger == null)
            //{
            mAnimator.ResetTrigger(SetTriger);
            if (TriggerName == "IsDead")
            {
                LastDeadNumberAnimation = DeadNumberAnimation;
                mAnimator.SetInteger("AttackNumber", (int)DeadNumberAnimation);
            }
            //} else {
            //    Debug.Log(LastTrggerName);    
            //}

            mAnimator.SetTrigger(TriggerName);
            LastTrggerName.Add(TriggerName);

            SetTriger = TriggerName;
        }*/

        SetSpeedCharacter();

    }

    void SetSpeedCharacter()
    {

        if (TargetAttack != null)
        {
            Speed = 4f;
        } 
        else
        {
            Speed = 1.5f;
        }

        mNavMeshAgent.speed = Speed;
        mNavMeshAgent.stoppingDistance = GetAttcakRange();
        mAnimator.SetFloat("Speed", Speed);
        TimeBetweenAtacks = GetTimeBetweenAtacks();


    }

    float GetAttcakRange()
    {
        float ReturnValue = 0f;

        if (mCharacterProperties.ThisZombie)
        {
            ReturnValue = mCharacterProperties.AttackRangeZombie;
        }
        else
        {
            ReturnValue = mCharacterProperties.AttackRangeUnarmed;
        }

        return ReturnValue;
    }

    int GetCountAttackAnimation()
    {
        int ReturnValue = 0;

        if (mCharacterProperties.ThisZombie)
        {
            ReturnValue = mCharacterProperties.CountAttackAnimationZombie;
        }
        else
        {
            ReturnValue = mCharacterProperties.CountAttackAnimationUnarmed;
        }

        return ReturnValue;
    }

    float GetTimeBetweenAtacks()
    {
        float ReturnValue = 0f;

        if (mCharacterProperties.ThisZombie)
        {
            ReturnValue = mCharacterProperties.AttackTimeZombie;
        }
        else
        {
            ReturnValue = mCharacterProperties.AttackTimeUnarmed;
        }

        return ReturnValue;
    }

    public void UpdateTarget()
    {
        if (TargetList.Count == 0)
        {
            CharacterMove(mEnemyAndResourcesGeneration.GetRandomPosition(1));
        }
        else
        {
            CharacterMove(TargetList[0].transform.position, TargetList[0].transform);
        }

    }

    public void AddInTargetList(CharacterProperties TargetCharacterProperties)
    {

        if (TargetList.Find(t => t == TargetCharacterProperties) == null)
        {
            TargetList.Add(TargetCharacterProperties);
        }

        UpdateTarget();

    }

    public void DeleteInTargetList(CharacterProperties TargetCharacterProperties)
    {
        TargetList.Remove(TargetCharacterProperties);

        UpdateTarget();

    }


}
