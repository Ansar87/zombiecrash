﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAndResourcesGeneration : MonoBehaviour
{

    LevelController mLevelController;

    Vector3 LeftCorner;
    Vector3 RightCorner;

    float MinX;
    float MaxX;

    float MinZ;
    float MaxZ;


    // Start is called before the first frame update
    void Start()
    {
        mLevelController = GetComponent<LevelController>();
        mLevelController.InitiateClass();

        LeftCorner = mLevelController.GetLowerLeftCorner();
        RightCorner = mLevelController.GetRightUpperCorner();

        MinX = LeftCorner.x;
        MaxX = RightCorner.x;

        MinZ = LeftCorner.z;
        MaxZ = RightCorner.z;

        GreateEnemy();
    }

    public void GreateEnemy()
    {

        int EnemyCount = 20;

        GameObject PrefabEnemy = mLevelController.GetPrefabCharacter(GamerTypeEnum.Enemy);
        Transform ParentTransform = mLevelController.GetParentNewObject(GamerTypeEnum.Enemy).transform;

        for (int i = 0; i < EnemyCount; i++)
        {

            GameObject NewEnemy = Instantiate(PrefabEnemy, ParentTransform);
            Vector3 RandomPosition = GetRandomPosition(15);
            NewEnemy.transform.SetPositionAndRotation(RandomPosition,Quaternion.identity);

            NewEnemy.GetComponent<CharacterProperties>().GamerType  = GamerTypeEnum.Enemy;
            NewEnemy.GetComponent<CharacterProperties>().ThisZombie = true;
            //NewEnemy.GetComponent<CharacterProperties>().SetMoveType();
        }

    }

    public Vector3 GetRandomPosition(float YPosition = 0)
    {
        Vector3 ReturnValue = new Vector3();
        ReturnValue = new Vector3(Random.Range(MinX, MaxX), YPosition, Random.Range(MinZ, MaxZ));
        return ReturnValue;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
